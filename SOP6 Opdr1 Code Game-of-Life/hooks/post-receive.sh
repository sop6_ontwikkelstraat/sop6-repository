#!/bin/sh
# Call Jenkins to start build and pass environment parameter
#
echo "executing post-receive hook"
echo "environment=testing"
echo "user=jenkins"

# cURL POST request using jenkins user with API token
curl --data "delay=0sec&environment=testing" \
    "http://localhost:8080/job/Game%20of%20life%20project_build/buildWithParameters"